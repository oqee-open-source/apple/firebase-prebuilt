// swift-tools-version:5.5
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "Firebase-Prebuilt",
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        
        .library(name: "FirebaseAnalytics", targets: [
            "Firebase",
            "FirebaseAnalytics",
            "FirebaseCore",
            "FirebaseCoreInternal",
            "FirebaseInstallations",
            "GoogleAppMeasurement",
            "GoogleAppMeasurementIdentitySupport",
            "GoogleUtilities",
            "FBLPromises",
            "nanopb"
        ]),
    
        .library(name: "FirebaseCrashlytics", targets: [
            "Firebase",
            "FirebaseCrashlytics",
            "FirebaseCoreExtension",
            "FirebaseSessions",
            "GoogleDataTransport",
            "Promises",
        ]),
        
    ],
    dependencies: [
        // Dependencies declare other packages that this package depends on.
        // .package(url: /* package url */, from: "1.0.0"),
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages this package depends on.
        
        .target(name: "Firebase"),
        
        .binaryTarget(name: "FirebaseAnalytics", path: "FirebaseAnalytics/FirebaseAnalytics.xcframework"),
        .binaryTarget(name: "FirebaseCore", path: "FirebaseAnalytics/FirebaseCore.xcframework"),
        .binaryTarget(name: "FirebaseCoreInternal", path: "FirebaseAnalytics/FirebaseCoreInternal.xcframework"),
        .binaryTarget(name: "FirebaseInstallations", path: "FirebaseAnalytics/FirebaseInstallations.xcframework"),
        .binaryTarget(name: "GoogleAppMeasurement", path: "FirebaseAnalytics/GoogleAppMeasurement.xcframework"),
        .binaryTarget(name: "GoogleAppMeasurementIdentitySupport", path: "FirebaseAnalytics/GoogleAppMeasurementIdentitySupport.xcframework"),
        .binaryTarget(name: "GoogleUtilities", path: "FirebaseAnalytics/GoogleUtilities.xcframework"),
        .binaryTarget(name: "FBLPromises", path: "FirebaseAnalytics/FBLPromises.xcframework"),
        .binaryTarget(name: "nanopb", path: "FirebaseAnalytics/nanopb.xcframework"),
        
        .binaryTarget(name: "FirebaseCrashlytics", path: "FirebaseCrashlytics/FirebaseCrashlytics.xcframework"),
        .binaryTarget(name: "FirebaseCoreExtension", path: "FirebaseCrashlytics/FirebaseCoreExtension.xcframework"),
        .binaryTarget(name: "FirebaseSessions", path: "FirebaseCrashlytics/FirebaseSessions.xcframework"),
        .binaryTarget(name: "GoogleDataTransport", path: "FirebaseCrashlytics/GoogleDataTransport.xcframework"),
        .binaryTarget(name: "Promises", path: "FirebaseCrashlytics/Promises.xcframework"),

    ]
)
