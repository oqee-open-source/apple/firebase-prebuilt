# Firebase-Prebuilt

A prebuilt version of Firebase Analytics and Firebase Crashlytics to improve app build times.

## How to update to a new version
Firebase prebuilt frameworks are distributed on the Releases page of the official GitHub project:
> https://github.com/firebase/firebase-ios-sdk/releases

You can download the asset `Firebase.zip` from the desired release and follow these steps:
1. Delete the folders `FirebaseAnalytics` and `FirebaseCrashlytics` from this local repository
2. Copy the folders `FirebaseAnalytics` and `FirebaseCrashlytics` from the downloaded archive to this local repository
3. Replace `Firebase.h` and `module.modulemap` in the folder `Sources/Firebase/include` with the ones from the downloaded archive
4. Update the file `Package.swift` with the frameworks added and removed in the new Firebase release
